import pydantic as pydantic


class WrongAction(Exception):
    pass


class WrongKey(WrongAction):
    """
    Неверный ключ для хода
    """


class WrongMove(WrongAction):
    """
    Ошибка при совершении хода
    """


class AlreadyEnded(WrongAction):
    """
    Игра уже окончена
    """


class ExceptionModel(pydantic.BaseModel):
    detail: str = pydantic.Field(..., title='Описание ошибки')
