import os

import fastapi

from config import Config
from game import TicTakToe
from model import Storage


async def storage_dep(request: fastapi.Request) -> 'Storage':
    return request.app.state.storage


def retrieve_game(name: str, storage: Storage) -> TicTakToe:
    if not storage.exists(name):
        raise fastapi.HTTPException(404, detail='Игра с указанным именем не найдена')
    return storage.games[name]


def token_dep(token_header: str = fastapi.Header(..., title='Токен авторизации', alias='api-token',
                                                 description='Токен предоставляется в формате '
                                                             '"Token <your_token>" (кавычки не нужны)')):
    auth_exception = fastapi.HTTPException(401, detail='Unauthorized')

    if 'Token' not in token_header:
        raise auth_exception
    else:
        user_token = token_header[6:]

        if user_token != Config.token:
            raise auth_exception
