import datetime
import random
import uuid
from enum import Enum
from typing import Tuple

from exceptions import WrongKey, WrongMove, AlreadyEnded


class GameStates(int, Enum):
    pending = 0
    in_game = 1
    ended = 2


class TicTakToe:
    def __init__(self, name: str):
        self.name = name
        self.state = GameStates.pending
        self.first_key = str(uuid.uuid4())[:8]
        self.second_key = str(uuid.uuid4())[:8]
        self.field = [[0 for i in range(3)] for j in range(3)]
        self.first_figure = random.choice(['x', 'o'])
        self.turn = 1
        self.winner = None
        self.created_at = datetime.datetime.now()

    def check_key(self, key: str):
        if self.turn == 1:
            if self.first_key != key:
                raise WrongKey("Неверный пользовательский ключ")
        if self.turn == 2:
            if self.second_key != key:
                raise WrongKey("Неверный пользовательский ключ")

    def make_move(self, key, pos: Tuple[int, int]):
        if self.check_ended():
            raise AlreadyEnded("Данная партия уже окончена")

        self.check_key(key)

        try:
            current_value = self.field[pos[0]][pos[1]]
            if current_value != 0:
                raise WrongMove("Клетка уже занята")
            else:
                self.field[pos[0]][pos[1]] = self.turn
                self.turn = 2 if self.turn == 1 else 1

        except IndexError:
            raise WrongMove("Выход за пределы поля")

        self.state = GameStates.in_game if not self.check_ended() else GameStates.ended

    def check_ended(self) -> bool:
        if player := self.check_grid(self.field):
            self.winner = player
            return True

    @staticmethod
    def check_grid(grid) -> int:
        # rows
        for x in range(0, 3):
            row = {grid[x][0], grid[x][1], grid[x][2]}
            if len(row) == 1 and grid[x][0] != 0:
                return grid[x][0]

        # columns
        for x in range(0, 3):
            column = {grid[0][x], grid[1][x], grid[2][x]}
            if len(column) == 1 and grid[0][x] != 0:
                return grid[0][x]

        # diagonals
        diag1 = {grid[0][0], grid[1][1], grid[2][2]}
        diag2 = {grid[0][2], grid[1][1], grid[2][0]}
        if len(diag1) == 1 or len(diag2) == 1 and grid[1][1] != 0:
            return grid[1][1]

        return 0
