FROM python:3.8-slim

RUN apt-get update && apt-get install -y libpq-dev gcc

WORKDIR /home/bot
COPY requirements.txt ./

RUN pip3 install -r ./requirements.txt

COPY . ./

ENTRYPOINT ["python3", "main.py"]
