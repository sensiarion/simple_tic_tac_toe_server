import os


class Config:
    port = int(os.environ.get('PORT', 5683))
    host = os.environ.get('HOST', '0.0.0.0')
    token = os.environ.get('API_TOKEN', 'pepa')
