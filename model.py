import datetime
from typing import Dict

from game import TicTakToe


class Storage:
    MAX_LIFETIME_MINUTES = 60

    def __init__(self):
        self.games: Dict[str, TicTakToe] = dict()

    def create_game(self, name: str) -> TicTakToe:
        if self.exists(name):
            raise ValueError("Игра с данным именем уже создана")

        self.games[name] = TicTakToe(name)

        return self.games[name]

    def get_games(self) -> Dict[str, TicTakToe]:
        """
        Перед выдачей все долго существующие игры удаляются
        """
        to_delete = []
        for name, game in self.games.items():
            game_lifetime = datetime.datetime.now() - game.created_at
            if game_lifetime > datetime.timedelta(minutes=self.MAX_LIFETIME_MINUTES):
                to_delete.append(name)

        for name in to_delete:
            self.games.pop(name)

        return self.games

    def exists(self, name: str) -> bool:
        return name in self.games

    def delete_game(self, name: str, key: str) -> bool:
        if game := self.games.get(name):
            if key == game.first_key or key == game.second_key:
                self.games.pop(name)
                return True
            else:
                return False
