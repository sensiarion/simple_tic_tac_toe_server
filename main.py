from typing import Literal, List, Tuple, Optional

import fastapi
import pydantic as pydantic
import uvicorn

from config import Config
from exceptions import WrongAction, ExceptionModel
from game import GameStates
from model import Storage
from utils import storage_dep, retrieve_game, token_dep

app = fastapi.FastAPI(title='Simple server',
                      description='Сервер для реализации игры в крестики нолики')

default_responses = {400: {'description': 'Игра с указанным именем не найдена', 'model': ExceptionModel},
                     401: {'description': 'Ошибка авторизации', 'model': ExceptionModel}}


class GameInfoCreate(pydantic.BaseModel):
    name: str
    state: GameStates = pydantic.Field(..., title='Состояние игровой сессии',
                                       description='0 – игра в ожидании хода, 1 – игра в процессе,'
                                                   ' 2 – игра окончена')
    winner: Optional[int] = pydantic.Field(..., title='Идентификатор победителя',
                                           description='При появлении победителя на карте в'
                                                       ' этом поле будет указан его идентификатор (1 либо 2)')

    class Config:
        orm_mode = True


@app.get('/games/tic_tac_toe', response_model=List[GameInfoCreate])
async def get_games(storage: Storage = fastapi.Depends(storage_dep),
                    token=fastapi.Depends(token_dep)):
    """
    Получение списка доступных в системе игровых сессий.

    Все игровые сессии длящиеся более 40 минут будут автоматически удалены
    """
    games = storage.get_games()
    return [GameInfoCreate.from_orm(i) for i in games.values()]


class GameInfoOut(GameInfoCreate):
    name: str
    state: GameStates
    turn: Literal[1, 2]
    field: List[List[int]] = pydantic.Field(..., title='Игровое поле',
                                            description='0 на игровом поле обозначает чистую клетку,'
                                                        ' первый и второй игроки заполняют поле'
                                                        ' значениями 1 и 2 соотствественно')
    first_figure: Literal['x', 'o'] = pydantic.Field(..., title='Фигура игрока, который ходит первым')


class GameInfoCreateOut(GameInfoOut):
    first_key: str = pydantic.Field(..., title='Ключ для ходов первого игрока')
    second_key: str = pydantic.Field(..., title='Ключ для ходов второго игрока')


@app.get('/games/tic_tac_toe/{name}', response_model=GameInfoOut, responses=default_responses)
async def get_game_info(name: str = fastapi.Path(...),
                        storage: Storage = fastapi.Depends(storage_dep),
                        token=fastapi.Depends(token_dep)):
    """
    Получение информации о ходе игры

    В поле **0** обозначает отсутствие фигуры, **1** – первый игрок, **2** – второй игрок
    """
    game = retrieve_game(name, storage)

    return GameInfoOut.from_orm(game)


@app.post('/games/tic_tac_toe/{name}', response_model=GameInfoCreateOut,
          responses={**default_responses,
                     409: {'description': 'Игра с указанным названием уже создана', 'model': ExceptionModel}})
async def create_game(name: str = fastapi.Path(...),
                      storage: Storage = fastapi.Depends(storage_dep),
                      token=fastapi.Depends(token_dep)):
    """
    Создание новой игровой сессии.

    При создании игры выдаются 2 ключа, которые используются для совершения ходов.
    Каждый из игроков должен использовать свой ключ, в свою очередь хода

    У каждой игровой сессии есть предельно допустимое время существования: 40 минут.
    """
    try:
        game = storage.create_game(name)
    except ValueError:
        raise fastapi.HTTPException(409, detail='Игра с подобным именем уже создана')

    return GameInfoCreateOut.from_orm(game)


class GameMoveParams(pydantic.BaseModel):
    player_key: str = pydantic.Field(..., title='Ключ игрока',
                                     description='Ключ игрока – ключ, получаемый при создании игры,'
                                                 ' по которому сервер идентифицирует игрока, '
                                                 'как участника определённой игры')
    move_position: Tuple[int, int] = pydantic.Field(..., title='Позиция для хода игрока')


@app.post('/games/tic_tac_toe/{name}/move', response_model=GameInfoCreateOut,
          responses={**default_responses,
                     400: {'description': 'Ошибка связанная с логикой игры', 'model': ExceptionModel}})
async def make_move(name: str = fastapi.Query(...),
                    storage: Storage = fastapi.Depends(storage_dep),
                    params: GameMoveParams = fastapi.Body(...),
                    token=fastapi.Depends(token_dep)):
    """
    Совершение хода
    """
    game = retrieve_game(name, storage)
    try:
        game.make_move(params.player_key, params.move_position)
    except WrongAction as e:
        raise fastapi.HTTPException(400, detail=str(e))

    return GameInfoCreateOut.from_orm(game)


@app.on_event('startup')
async def on_startup():
    app.state.storage = Storage()


if __name__ == '__main__':
    uvicorn.run(app, host=Config.host, port=Config.port)
